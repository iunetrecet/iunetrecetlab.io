---
layout: post
title: "Diseinua grafikorako tresna askeak I"
thumbnail: "images/posts/tresnaki/tresnaki.png"
category: "iritzia"
date: 2020-11-06
author: Iune Trecet
---

Diseinuan badago Adobez kanpoko mundua. Tresna askeen artean diseinurako programa aurreratuak daude, erabilerrazak direnak, doan eskuratu ditzakezunak eta gainera euskaraz.
Lehenik eta behin, ezagunena, **GIMP** da. Pintzela ahoan duen Wilber azeritxoa logo duen programa hau, argazkiak moldatzeko erabiltzen da. Mugikorrarekin edo argazki kamerarekin egindako argazki horiek edertzeko aukera izango duzu, koloreak indartu, argitu, nahi duzun tamainan jarri edota begi zuloak argitzeko erabili daiteke, beste aukera askoren artean. Nola erabiltzen den ikasteko modurik hoberena, argazki bat GIMPen ireki, eta dituen tresna guztiekin saltseatzen joatea da. Gauza errealistak egin ditzakezu, baina baita argazkiari ikutu arraroak eman ere. GIMPek pixelez sortutako irudiak erabiltzen ditu, kolore zehatz bat duen karratuekin (pixelak) sortutako sareta bat da pantailan ikusten dugun argazkia, geroz eta pixel gehiago izan, geroz eta informazio gehiago edukiko dugu eta irudiaren kalitatea handiagoa izango da. Baina ez kezkatu, gure gailuek, normalean, behar ditugunak baino pixel gehiago edukitzen dituzte eta. Programa, gimp.org webgunean duzu eskuragarri.

Nire ordenagailuan ezinbestekoa dena, **Inkscape** da. Grafiko bektorialekin lan egiteko programa da. GIMPek pixelak erabiltzen dituen bitartean, Inkscapek puntuak eta puntuak lotzeko bektoreak erabiltzen ditu, eta horrela, ez dugu izango irudiak eskalatzeko inongo mugarik. Puntuen eta bektoreen gaia arrotza egin daiteke, baina azken finean arkatz batez egin dezakegun marra hori irudikatzea da errazena. Inkscapen bidez, edozein tamainatako kartelak egin ditzakegu, aplikazio desberdinak izan ditzaken enpresa bateko logoa edo guaixeko astekarirako portada polit bat. Programak nola funtzionatzen duen ikasteko modu polit bat tutorial hau jarraitzea da; hauxe da nire proposamena: https://labur.eus/InscapeTutoriala. Aukera pila bat dituen programa bat da, eta probatzea gomendatzen dizuet. Inkscape deskargatzeko aukera, inkscape.org-en aurkituko duzue.
Datorren kolaborazioan, diseinurako tresna askeen gomendioekin jarraituko dut.

