---
layout: post
title: "Ezkontza gonbidapena"
thumbnail: "images/posts/gonbidapena/gonbidapena.png"
category: "grafikoa"
date: 2021-01-11
author: Iune Trecet
---

Lagun batzuen ezkontzarako egindako gonbidapena.

<img src="/images/posts/gonbidapena/gonbidapena.jpg" alt="Gonbidapena"/>
