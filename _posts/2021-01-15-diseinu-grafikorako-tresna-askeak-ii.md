---
layout: post
title: "Diseinu grafikorako tresna askeak II"
thumbnail: "images/posts/tresnaki/tresnakii.png"
category: "iritzia"
date: 2021-01-15
author: Iune Trecet
---

Urte berriarekin batera beti datoz helburuak, eta hauen artean klasiko bat zerbait berria ikastea izaten da. Nire aurtengo helburuen zerrenda honetan diseinu grafikorako programa aske hauek erabiltzen ikastea da.

Lehenengoa, Scribus, maketaziorako erabiltzen den programa da. GIMP eta Inkscape irudien lanketara zuzenduta dauden moduan, Scribus testuaren konposiziora zuzenduta dago. Liburuak, liburuxkak, buletinak edo aldizkariak sor daitezke programa honetan. CMYK koloreetako kudeaketa duenez, oso erabilgarria da dokumentuak inprentara bidaltzeko. Eta horretaz gain, euskaraz aurki dezakegu. Gutxitan erabili izan dut programa hau, baina aurten erabilpena ikasi nahiko nuke, bere baliokide pribatiboa alde batera uzteko. Programa scribus.net webgunean lortu daiteke, edo noski, GNU/Linuxeko programa biltegietan.

Krita da erabiltzen ikasi nahi dudan hurrengoa. Potentzia handiko programa honek pintura digitala eta ilustrazioa ditu helburu. Kritaren oinarrian, zerotik egindako marrazkiak dira, bai bitmapak bai bektoreak erabiliz. Izugarrizko pintzel liburutegi handia dauka, akuarela, olioak, akrilikoak, arkatzak edo ikatzak adibidez aurki ditzakezu; honek ilustrazioak sortzeko aukera ederra ematen du. Eskuz egindako marrazkiak digitalean egitea erronka polita da 2021erako. Eredu politak ikusgarri daude krita.org webgunean, bilatzera animatzen zaituztet eta programa ere eskura dago.

Egiteke ditudan gauzen zerrendan beti agertzen dena Blender da. Hiru dimentsioko irudiak egiteko eta hauek animatzeko erabiltzen da batez ere. Formak, Inkscapen bezala, bektoreen bitartez egiten dira, baina planoan izan beharrean hiru dimentsiotan modelatu behar dira, bolumenarekin. Hori da zailtasun handiena. Bideo ediziorako erabili izan dut, baina 3D-an irudiak sortzeko denborarik ez dut hartu. Diseinu askean dagoen programa aurreratuenetako bat da, eta maila profesionalean asko erabili izan da 3D-ak egiteko, adibidez, Spider-Man 2 edo Marvelen Captain America pelikulak egiteko. Blender.org webgunean dago eskuragarri.

<a
href="https://guaixe.eus/komunitatea/iune/1610636077654-diseinu-grafikorako-tresna-askeak-ii">Guaixen
publikatutako artikulua</a>

