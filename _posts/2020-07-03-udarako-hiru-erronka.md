---
layout: post
title: "Udarako hiru erronka"
thumbnail: "images/posts/udakoerronkak/udakoerronkak.png"
category: "iritzia"
date: 2020-07-03
author: Iune Trecet
---


Uztaila bat-batean iritsi da gurera, 2020 urteko 4 hilabete arraro hauen ondoren, eta uda betean sartu gara tenperatura altuekin, baina festa faltarekin. Ezohiko uda honetan sormena erabili behar izango dugu planak egiteko garaian, eta nik erronka hauek proposatzen dizkizuet, ongi pasa dezazuen.

1. **Wikimediaren argazki lehiaketa eta Euskal Herria ezagutzeko bidaia** [https://labur.eus/wikiBizi](https:labur.eus/wikiBizi):
Euskarako wikilariek WikiBizi proiektua jarri dute martxan. Proiektu honetan, udaz gozatzeko bi aukera polit daude. Lehenengoa, hiru atal dituen argazki lehiaketa da; ondarea, natura edo kultura gaiak hizpide hartuta, eskatzen diguten argazkia ateratzea, adibidez, Euskal Herriko monumentu baten argazkia izan daiteke, zein da Sakanako monumentu politena? Sari bikainak daude. Eta, gainera, gizateriaren argazki artxibo aske handiena betetzen lagunduko duzu. Bigarren aukeran Euskal Herria ezagutzeko gida irekia sortzen ari dira, ibilbide sorta handi batekin. Euskal Herriko txoko ederrenak ezagutzeko aukera ezin hobea eskaintzen digute. Erronka: egin ezazu ibilbide bat eta egin ezazu argazki polit bat lehiaketarako. Guaixek uda guztietan egiten duen argazki lehiaketarako ere erabil dezakezu ibilbidea.

2. **Geocaching altxorraren bila** [https://geocaching.com](https://geocaching.com):
Geocaching mundu mailako altxor bilaketa jolasa da. Munduan zehar altxor pila bat daude, ezkutatuta, cacheak deiturikoak; eta pista batzuk jarraiki, plan ederra da horiek aurkitzera joatea, batez ere haurrekin egiten baduzu. Behin altxorra aurkituta, barruan cachearen ezaugarriak egongo dira eta askotan elementu bat aurkituko duzu, gauza txiki bat. Honen helburua elementua hartzea eta beste geocacheren batean uztea izango da, horrela munduan barna bidaiatu dezan. Nik adibidez, Arabako Lautadan hartutako garagardo botila baten txapa bat Eskoziako mendi batera eraman nuen, baina ez du zergatik hain urruti izan, ondoko geocachera ere eraman daiteke. Erronka: Sakanan hamar cache baino gehiago daude. Aurki itzazu, ahal bada, bi gutxienez. 

3. **Kaleko tipografia:**
Gure kaleetan eta naturan milaka forma daude, eta horien artean letrak ere badaude. Adibidez, autoen gurpilak "o" dira, eta farola batek "t" forma izan dezake. Erronka: aurki ezazu zure inguruan alfabeto osoa eta poster bat egin ezazu kaleko alfabeto guztiarekin, zure tipografia propioa sortuz.

Disfrutatu uda!

