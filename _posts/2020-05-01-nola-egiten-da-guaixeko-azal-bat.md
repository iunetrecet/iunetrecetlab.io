---
layout: post
title: "Nola egiten da Guaixeko azal bat?"
thumbnail: "images/posts/M1/M1.png"
category: "grafikoa"
date: 2020-05-01
author: Iune Trecet
---

Ikusi nola sortzen den astero Guaixeko astekariko azala. Aste honetan maiatzak 1eko azalak Rodchenkoren sorkuntzari omenaldia egin dio

<iframe width="560" height="315" src="https://www.youtube.com/embed/It_2ugzdyPY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
