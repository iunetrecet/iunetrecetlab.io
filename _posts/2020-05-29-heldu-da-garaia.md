---
layout: post
title: "Heldu da garaia"
thumbnail: "images/posts/garaia/garaia.png"
category: "iritzia"
date: "2020-05-29"
author: Iune Trecet
---

Konfinamenduko egun hauek pantaila bati begira pasa ditugu; pantaila izan da munduaren leihoa. Errealitatearekin izan dugun lotura handiena hori izan da. 

Telelanean ibili garenok, ordenagailuaren pantaila izan da seguruenik gehien erabili duguna. 8 orduz, gutxienez, honen aurrean izan gara. Batzuk hobe moldatu dira eta beste batzuk okerrago, bakoitzaren etxeko egoerak eragin handia izan baitu honetan. Ez da berdina gela bat edukitzea lan eremu moduan erabiltzeko, edo gela hori etengabeko arreta eskatzen duten bi haur txikirekin elkarbanatzea. Telelana gelditzeko etorri den zerbait bada, egoera hauei erreparatzea eta psikologikoki dituen eraginak aztertzea beharrezkoa izango litzateke.

Beste askoren enplegua etenda izan da, ABEEak edo kaleratzeak direla eta. Osasun krisiaren aurretik denbora librerik ez zuten pertsonak, orain denbora beste gauzetan pasatzen ikasi behar izan dute. Friki, hacker eta gamerrek ez bezala, pertsona gehienek aisialdia kalean, tabernan edo mendian pasatzen zuten, eta alarma egoeraren ondoren errotik aldatu behar izan dute. Etxean zituzten gauzekin pasa behar izan dute itxialdia eta gehienek ere pantaila bat izan dute bidelagun. Ordenagailuen edo mugikorren erabilera izugarri igo da egun hauetan. Batzuek ikasteko erabili dute denbora, online dauden ikastaroen bitartez, beste batzuek beraien jakituria elkarbanatu dute ikastaro propioak egiten. Plataforma digitaletako telesailak irensten pasa dituzte askok. Mugikorraren bidez etxean egindako sormenen eta burutazioen zabalpena sare sozialen bidez nazkatzera arte zabaltzen ibili da jendea asko. Eta kalitate hobereneko eta pribatutasuna mantentzeko bideo dei onena non egin aztertzen egon denik ere bada.

Haurrak pantaila bati lotuta izan dira egun guzti hauetan ere, eskolak Internet bidez jarraitu behar izan dituzte, marrazki bizidunak ikusi edo zunba dantzan ibili dira ia egunero pantailaren aurrean. 

Normalean, gailuen erabilera arduratsu baten alde idazten dut zutabe honetan, Kilker Hiztunarena eginez. Baina oraingo honetan arduratsuena pantailen aurretik alde egiteko deia luzatzea da, ahal den heinean noski. Heldu da garaia, atera kalera eta komunikatu zaitezte zuen bizilagun, familia edo kaleko edozein pertsonekin, beti ere osasun neurriak errespetatuz. Egin korrika, poteatu, eguzkitan ibili, irri egin... utzi alde batera pantailatzar horiek eta erlazionatu zaitezte mundu fisikoan. Gure buruek eskertuko dute!

<a
href="https://guaixe.eus/komunitatea/iune/1590484043987-heldu-da-garaia">Guaixen
publikatutako artikulua</a>
