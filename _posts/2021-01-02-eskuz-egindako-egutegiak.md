---
layout: post
title: "Urtarrila | Eskuz egindako egutegia"
thumbnail: "images/posts/egutegiak/urtarrila.png"
category: "diy"
date: 2021-01-01
author: Iune Trecet
---

Urtarrileko portadak lagunentzat egindako egutegietan.

<img src="/images/posts/egutegiak/Urtarrila1.jpg" alt="Urtarrila" />
<img src="/images/posts/egutegiak/Urtarrila2.jpg" alt="Urtarrila" />
<img src="/images/posts/egutegiak/Urtarrila3.jpg" alt="Urtarrila" />
<img src="/images/posts/egutegiak/Urtarrila4.jpg" alt="Urtarrila" />
<img src="/images/posts/egutegiak/Urtarrila5.jpg" alt="Urtarrila" />
<img src="/images/posts/egutegiak/Urtarrila6.jpg" alt="Urtarrila" />
