---
layout: post
title: "Uda Sakanan"
thumbnail: "images/posts/udasakanan/udasakanan.png"
category: "grafikoa"
date: 2020-07-31
author: Iune Trecet
---

Guaixe astekariko udako berezirako egindako azala.

<img src="/images/posts/udasakanan/udasakanan.jpg" alt="Uda Sakanan azala">
