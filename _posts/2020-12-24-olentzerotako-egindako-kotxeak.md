---
layout: post
title: "Olentzerotako egindako kotxeen irudiak"
thumbnail: "images/posts/kotxeak/kotxeak.png"
category: "grafikoa"
date: 2020-12-24
author: Iune Trecet
---

Paretean jartzeko 1,5 metrotako posterra, bikotearen kotxeekin.

<img src="/images/posts/kotxeak/kotxeak.svg" alt="Kotxeak"/
