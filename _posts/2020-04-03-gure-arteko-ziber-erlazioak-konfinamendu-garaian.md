---
layout: post
title: "Gure arteko ziber-erlazioak konfinamendu garaian"
thumbnail: "images/posts/zibererlazioak/zibererlazioak.png"
category: "iritzia"
date: 2020-04-03
author: Iune Trecet
---

Pertsonen arteko erlazioak Internet bidez egiten diren honetan, telelana dela, ikasleentzako eskolak direla, mediku kontsultak direla edo lagunekin egiten duzun ziberpoteoa dela, kontuan izan behar dugu datu eta informazio hori guztia zeinen eskuetatik pasatzea nahi dugun. 
Hezkuntzan, Sillicon Valleyko erraldoi teknologikoari gure haurren datu pila bat oparitzen ari gatzaizkio. Datu hauekin negozioa egiten duten aplikazio eta guneak berresten dira kolaboratzaile eta askeak diren programen aurrean. Moodle, Nextcloud edo Jitsi bezalako tresnak, adibidez, haurren datuekin negoziorik ez egiteaz gain, gure beharretara egokitu ditzakegu, eta gainera, euskarazko bertsioak dituzte (eta ez edukiz gero, euskaratu daitezke). Ikastetxeetako informatikari librezaleei ere, nire 8tako txaloaren zati bat bidaltzen diet.
Telelana egiten dugunok ere, badugu gure alternatiben zerrenda. Guaixeren lana antolatzeko garaian, saiatu gara irizpide hauek kontuan hartzen, eta horregatik, erredakzioko bilerak Jitsi bidez ari gara egiten eta orri banaketa Riseup-eko pad batean egin dugu. Gainera, argazki guztiak Goienak, Tokikom eta Infosarerekin batera sortutako Tokimediaren bitartez gordetzen ditugunez, beti eskura ditugu, gure zerbitzari propioan. Badakit, badaudela enpresak Nextcloud bat instalatu dutenak beraien zerbitzarietan, dokumentu, argazki edo fitxategi guztiak bertan edukitzeko.
Aisiaren inguruan ere buelta asko eman ditzakegu; aisiaren eredua aldatu da, hori argi dago, inoiz baino gutxiago "pirateatzen" dugu, baina hori beste baterako utziko dut. Gure artean erlazionatzeko modua begiratzen badut, ia guztiek erabiltzen dituzue/ditugu erraldoi teknologikoek gure eskura jartzen dituzten tresnak, informazio hori guztia beraien eskutan jarriz. Eta hauek ere, birpentsatu beharko genituzke (Riot, Jitsi, xmpp...). Guk lagun artean adibidez, lehengo larunbatean, sagardotegiko zita bertan behera gelditu zenez, Ziber-txotx bat egin genuen Jitsi bidez, eta egia esan, denen aurpegiak ikusi ahal izateak, lagunak gertuago sentiarazi zidan, indarrak hartu nituen.
Hau guztia  aurrera eraman ahal izateko, eta hurrengo krisia gainean izan aurretik, garrantzitsuena da erakunde publikoek tresna askeen aldeko benetako apustu bat egitea. Gure datuak eta gure txikienenak, gure eskuetan egon daitezela eta ez dezala inork gure bizitzekin negozioa egin.
Eta garrantzitsuena, zaindu dezagun gure burua, gureekin dugun erlazioa eta ziber-txotx asko egin!

<a
href="https://guaixe.eus/komunitatea/iune/1585814976879-gure-arteko-ziber-erlazioak-konfinamendu-garaian">Guaixen
publikatutako artikulua</a>
