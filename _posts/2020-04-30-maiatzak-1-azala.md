---
layout: post
title: "Maiatzak 1 azala"
thumbnail: "images/posts/M1/M1azala.png"
category: "grafikoa"
date: 2020-04-30
author: Iune Trecet
---

2020ko maiatzak 1erako egindako azala, Rodchenkoren sorkuntzari omenaldia egin zion.

<img src="/images/posts/M1/M1azala.jpg" alt+"Maiatzak 1 azala"/>

